const mongoose = require('mongoose');
const crypto = require('crypto');


const userSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    email:{
        type:String,
        unique:true,
        required:true
    },
    name: {
        type: String,
        required: true
    },
    hashPassword: {
        type: String,
        required: true
    },
    salt: {
        type: String,
        required: true
    },
    token: {
        type:String,
        default:""
    },
    date:{
        type:Date,
        default:new Date(2011, 0, 1, 0, 0, 0, 0),
    }
},{
    collection: "User",
    versionKey: false
});


userSchema.virtual('password')
    .set(function (password) {
        this.salt = Math.random() + 'dhfjgbjkcdngvkjnbjfdgbvkigrisjsjgibgfidri\vgkli\adgfsgsgs';
        this.hashPassword = this.encryptPassword(password);
    });


userSchema.methods = {
    encryptPassword: function (password) {
        return crypto.createHmac('sha256', this.salt).update(password).digest('hex');
    },
    checkPassword: function (password) {
        return this.encryptPassword(password) === this.hashPassword;
    }
};

userSchema.statics = {
    checkEmail: async function (email) {

        const _email = await this.findOne({email: email}).exec();
        if (_email === null){
            return true;
        } else {
            throw Error('Почта уже зарегистрирована!');
        }
    },
    checkPassword: async function (password) {
        if (password.length < 6){
            throw Error('Пароль должен быть больше 8 символов!');
        } else {
            return true;
        }
    },
    checkName: async function (name) {
        if (name.length < 3){
            throw Error('Длинна имени должна быть больше 2 символов!');
        } else {
            return true;
        }
    },
};

userSchema.pre('save', function(next) {
    this._id = new mongoose.Types.ObjectId();
    
    next();
});

module.exports = mongoose.model('User', userSchema);