const User = require('./../db/Schma');
const jwt = require('jsonwebtoken');
const config = require('./../libs/config');
const log = require('./../libs/log')(module);

module.exports = function (req, res) {
    User.findOne({token: req.body.token}).exec()
    .then(payload => {
        res.header('Content-Type', 'text/plain');
        res.status(200).send("Все окей, ты авторизирован");
    })
    .catch(err => {
        res.header('Content-Type', 'text/plain');
        res.status(400).send("Ты еще не вошел\n"+err.message);
    });
};