const express = require('express');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
var http = require('http');
var https = require('https');
var path = require('path');
var compression = require('compression');//Модуль отвечающий за компрессию сервера
var config = require('./libs/config');
var session = require('express-session');
var multer = require('multer');//??
var log = require('./libs/log')(module);
var fs = require('fs');



const app = express();
app.use(compression());

const key = fs.readFileSync('/etc/letsencrypt/live/www.ducker-studio.com/privkey.pem');
const cert =fs.readFileSync('/etc/letsencrypt/live/www.ducker-studio.com/cert.pem');

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Credentials", "true");
    res.header("Access-Control-Allow-Headers", "Content-Type");
    next();
});
// app.set('view engine', 'ejs');
app.use(bodyParser.urlencoded({
    extended: true,
    limit: '50mb'
}));
app.use(cookieParser());

    app.use(multer(
        {
            dest: path.join(__dirname, 'public/uploads'),//папка для хранения фронта (картинки изображения и тд.)
            limits: {
                fieldNameSize: 999999999,
                fieldSize: 999999999
            }
        }
    ).any());

app.use('/files', express.static('../files'));//Обработка адреса http://localhost/files/название файла из папки files

var sessionStore = require('./libs/sessionStore');//настройка хранилища сессий
app.use(session({///настройка сессии
    secret: config.session.secret,
    key: config.session.key,
    cookie: config.session.cookie,
    store: sessionStore
}));
//************************* Routes ***********************************
require('./routes')(app);//настройка роутов сайта

//************************* 404 ***********************************
app.use(function(req, res){
   res.send('404 ошибка');
});

//************************* Запуск сервера ***********************************
if(config.http==="http"){
    var httpServer = http.createServer(app);//Запуск HTTP сервера, не HTTPS!!!
}else{
    var httpServer = https.createServer({ key:key, cert:cert },app);
}
function onListening(){
    log.info('Listening on port ', config.port);
}
httpServer.on('listening', onListening);
httpServer.listen(config.port, config.ip);